﻿using System;

namespace graphikSem
{
    public class TreeSortOnArray<T> where T : IComparable<T>
    {
        //only for testing
        private static long Iterations;

        private int Count { get; set; }

        //(data, parrent, leftleaf, rightleaf)
        private (T, int, int, int)[] heap;

        /// <summary>
        /// Sorts array
        /// </summary>
        /// <param name="array"></param>
        //Initial sort procedure
        public static void Sort(T[] array)
        {
            if (array == null || array.Length == 0)
                return;
            var sorter = new TreeSortOnArray<T>(array.Length);
            foreach (var item in array)
            {
                sorter.Insert(item);
            }
            var visitor = new TreeVisitor(array);
            sorter.Traverse(0, visitor);
        }

        TreeSortOnArray(int size)
        {
            //only for preparing data
            Iterations = 0;
            Count = 0;
            heap = new (T, int, int, int)[size];
        }

        private void Insert(T item)
        {
            if (Count == 0)
            {
                heap[Count] = (item, -1, -1, -1);
                Count++;
                return;
            }
            int index = 0;
            bool lessThanParrent = true;
            while (true)
            {
                Iterations++;

                int parrent = index;
                if (item.CompareTo(heap[index].Item1) < 0)
                {
                    index = heap[index].Item3;
                    lessThanParrent = true;
                }
                else
                {
                    index = heap[index].Item4;
                    lessThanParrent = false;
                }
                if (index == -1)
                {
                    AddToFolder(item, parrent, lessThanParrent);
                    break;
                }
            }
        }

        private void AddToFolder(T item, int parrent, bool lessThanParrent)
        {
            if (lessThanParrent)
            {
                heap[parrent] = (heap[parrent].Item1, heap[parrent].Item2, Count, heap[parrent].Item4);
            }
            else
            {
                heap[parrent] = (heap[parrent].Item1, heap[parrent].Item2, heap[parrent].Item3, Count);
            }
            heap[Count] = (item, parrent, -1, -1);
            Count++;
        }

        private void Traverse(int i, TreeVisitor visitor)
        {
            Iterations++;

            if (i == -1)
                return;

            Traverse(heap[i].Item3, visitor);

            visitor.Fill(heap[i].Item1);

            Traverse(heap[i].Item4, visitor);
        }
        
        /// <summary>
        /// Returns static Iterations Count for preparing data
        /// </summary>
        /// <returns></returns>
        public static long GetIterationsCount()
        {
            return Iterations;
        }

        /// <summary>
        /// Instance is used to update array while Travesrsing 
        /// </summary>
        class TreeVisitor
        {
            private T[] array;
            private int index;
            public TreeVisitor(T[] array)
            {
                this.array = array;
            }

            public void Fill(T item)
            {
                array[index] = item;
                ++index;
            }
        }
    }
}