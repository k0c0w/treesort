﻿using System;
using System.Windows.Forms;

namespace graphikSem
{
    internal static class Program
    {
        [STAThread]

        public static void Main()
        {
            EntryMessage();

            Console.WriteLine("Enter full path to data directory:");
            string pathToData = Console.ReadLine();
            GraphForm.PrepareDataForForms(pathToData);


            Application.Run(GraphForm.GetIterationsGraphForm());
            Application.Run(GraphForm.GetTimeGraphForm());
        }

        static void EntryMessage()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("To work correctly, please, generate data with FileGenerator.cs\nEnter input correctly.");
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
