﻿using System;

namespace graphikSem
{
    public class MyList<T>
    {
        //only for preparing data
        private static long Iterations;

        private string exeptionMessage = "MyList does not contain such index";

        private Node Head;
        private Node Last;

        public int Count { get; private set; }

        public MyList()
        {
            Iterations = 0;
        }

        public void Add(T item)
        {
            if (Head == null)
            {
                Head = new Node(item);
                Last = Head;

            }
            else if (Count == 1)
            {
                Last = new Node(item);
                Head.Next = Last;
            }
            else
            {
                var last = new Node(item);
                Last.Next = last;
                Last = last;
            }
            Count++;
        }

        public T this[int index]
        {
            get
            {
                if (IsIncorrectIndex(index))
                    throw new IndexOutOfRangeException(exeptionMessage);
                return FindNode(index).Data;
            }

            set
            {
                if (IsIncorrectIndex(index))
                    throw new IndexOutOfRangeException(exeptionMessage);
                var current = new Node(value);
                if (index == 0)
                {
                    current.Next = Head.Next;
                    Head = current;
                }
                else if (index == Count - 1)
                {
                    var prenode = FindNode(index - 1);
                    prenode.Next = current;
                    Last = current;
                }
                else
                {
                    var prenode = FindNode(index - 1);
                    var old = prenode.Next;
                    prenode.Next = current;
                    current.Next = old.Next;
                }
            }
        }

        private Node FindNode(int index)
        {
            Node current = Head;
            for (int i = 0; i < index; i++)
            {
                current = current.Next;

                //only for preparing data
                Iterations++;
            }
            return current;
        }

        private bool IsIncorrectIndex(int index)
        {
            return index >= Count || index < 0;
        }

        /// <summary>
        /// Returns static Iterations Count for Series
        /// </summary>
        /// <returns></returns>
        public static long GetIterationsCount()
        {
            return Iterations;
        }

        class Node
        {
            public T Data { get; }

            public Node Next { get; set; }

            public Node(T item)
            {
                Data = item;
            }
        }
    }
}