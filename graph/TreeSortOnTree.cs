﻿using System;

namespace graphikSem
{
    public class TreeSortOnTree<T> where T : IComparable<T>
    {
        //Only for preparing data
        private static long Iterations;

        TreeSortOnTree<T> RightLeaf { get; set; }
        TreeSortOnTree<T> LeftLeaf { get; set; }

        public T current { get; private set; }
        /// <summary>
        /// Represents a segment of tree 
        /// with <paramref name="item"/> in root
        /// </summary>
        /// <param name="item"></param>
        private TreeSortOnTree(T item)
        {
            current = item;
        }

        /// <summary>
        /// Sorts array
        /// </summary>
        /// <param name="array"></param>
        // Initial sort procedure
        public static void Sort(T[] array)
        {
            if (array == null || array.Length == 0)
                return;
            var length = array.Length;
            var mainTree = new TreeSortOnTree<T>(array[0]);
            for (int i = 1; i < length; i++)
            {
                mainTree.Add(array[i]);
            }
            var visitor = new TreeVisitor(array);
            mainTree.Traverse(visitor);
        }

        private void Traverse(TreeVisitor visitor)
        {
            if (LeftLeaf != null)
                LeftLeaf.Traverse(visitor);

            visitor.Fill(current);
            //only for preparing data
            Iterations++;

            if (RightLeaf != null)
                RightLeaf.Traverse(visitor);
        }

        private void Add(T item)
        {
            var nextTree = new TreeSortOnTree<T>(item);
            this.Insert(nextTree);
        }

        private void Insert(TreeSortOnTree<T> tree)
        {
            //Only for preparing data
            Iterations++;

            if (tree.current.CompareTo(current) < 0)
                if (LeftLeaf != null)
                    LeftLeaf.Insert(tree);
                else LeftLeaf = tree;
            else
               if (RightLeaf != null)
                RightLeaf.Insert(tree);
            else RightLeaf = tree;
        }

        /// <summary>
        /// Class instance is used to update array
        /// while Traversing
        /// </summary>
        class TreeVisitor
        {
            T[] array;
            int index;

            public TreeVisitor(T[] array)
            {
                this.array = array;
            }

            public void Fill(T item)
            {
                array[index] = item;
                ++index;
            }
        }

        /// <summary>
        /// Returns static Iterations Count
        /// </summary>
        /// <returns></returns>
        public static long GetIterationsCount()
        {
            var result = Iterations;
            Iterations = 0;
            return result;
        }
    }
}
