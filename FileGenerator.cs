﻿
namespace FilesGenerations
{
    internal class Program
    {
        static void Main()
        {
            var generator = new FileGenerator(RequestPath());
            generator.CreateData();
        }

        static string RequestPath()
        {
            Console.WriteLine("Enter destination directory:");
            return Console.ReadLine()!;
        }
    }

    internal class FileGenerator
    {
        private Random random;
        private string path;
        public FileGenerator(string pathToDirectory)
        {
            if(string.IsNullOrEmpty(pathToDirectory))
            {
                pathToDirectory = Directory.GetCurrentDirectory();
                Console.WriteLine("Error! Standart project directory has been chosen.");
            }
            this.path = pathToDirectory;
            random = new Random();
        }

        public void CreateData()
        {
            for(int symbolCount = 100; symbolCount <= 10000; symbolCount += 100)
            {
                for(int repetition = 1; repetition <= 50; repetition++)
                {
                    var pathToFile = String.Format(@"{0}\{1}_{2}.txt", path, symbolCount, repetition);
                    var r = File.Create(pathToFile);
                    r.Close();
                    using (StreamWriter sw = File.AppendText(pathToFile))
                    {
                        int current = 0;
                        while (current < symbolCount)
                        {
                            int l = random.Next(2);
                            var number = l == 1 ? -random.Next(): random.Next();
                            sw.WriteLine(String.Format("{0}", number));
                            current++;
                        }
                    }
                }
            }
        }
    }
}
